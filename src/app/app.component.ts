import { Component, OnInit } from '@angular/core';
import { TestServiceService } from './services/test-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'bevit';
  constructor(private testService:TestServiceService){}

  ngOnInit(){

 this.testService.testBootstrap().subscribe(
   response => { this.title =  response}, 
   error => {this.title = "Qualcosa non ha funzionato. Controlla il backend"});
  }
}
