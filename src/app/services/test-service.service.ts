import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TestServiceService {

  constructor(private httpClient:HttpClient) { }

  public testBootstrap(){
    return this.httpClient.get(environment.testUrl).pipe(map(response => {return response as string}));
  }
}
